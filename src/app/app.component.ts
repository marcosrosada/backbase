import { Component } from '@angular/core';
import { fadeAnimation } from './shared/utils/fadeAnimation';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
}
