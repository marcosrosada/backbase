import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';

const appRouter: Routes = [
    // { path: '', component: HomeComponent },
    // { path: 'nao-autorizado', component: NotAllowedComponent },
    { path: '', loadChildren: './weather/weather.module#WeatherModule' },
    // { path: 'grupos-aprovacao', loadChildren: './approval-group/approval-group.module#ApprovalGroupModule' }
  ];

@NgModule({
  imports: [RouterModule.forRoot(appRouter, { useHash: false })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
