import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { environment } from './../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(private http: HttpClient) { }


  onGetWeatherList(city: string, country: string) {
    return this.http.get(`${environment.API_URL}weather?q=${city},${country}&appid=${environment.APP_ID}`);
  }
  

  onGetForecastNextHours(city: string, country: string) {
    return this.http.get(`${environment.API_URL}forecast?q=${city},${country}&appid=${environment.APP_ID}`);
  }
}
