import { Main } from './main.model';
import { Sys } from "./sys.model";
import { Wind } from "./wind.model";
import { Clouds } from "./clouds.model";
import { Weather } from './weather.model';

export class List {
    constructor(
        public dt?: number,
        public main?: Main,
        public weather?: Weather[],
        public clouds?: Clouds,
        public wind?: Wind,
        public sys?: Sys,
        public dt_txt?: string
    ) { }
}