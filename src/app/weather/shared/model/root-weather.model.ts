import { Coord } from "./coord.model";
import { Weather } from "./weather.model";
import { Main } from "./main.model";
import { Wind } from "./wind.model";
import { Clouds } from "./clouds.model";
import { Sys } from "./sys.model";

export class RootWeather {
    constructor(
        public coord?: Coord,
        public weather?: Weather[],
        public base?: string,
        public main?: Main,
        public visibility?: number,
        public wind?: Wind,
        public clouds?: Clouds,
        public dt?: number,
        public sys?: Sys,
        public id?: number,
        public name?: string,
        public cod?: number,
    ) { }
}