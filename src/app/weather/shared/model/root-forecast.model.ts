import { City } from "./city.model";
import { List } from "./list.model";

export class RootForecast {
    constructor(
        public cod?: string,
        public message?: number,
        public cnt?: number,
        public list?: List[],
        public city?: City
    ) { }
}