export class Sys {
    constructor(
        public type?: number,
        public id?: number,
        public message?: number,
        public country?: string,
        public sunrise?: number,
        public sunset?: number
    ) { }
}