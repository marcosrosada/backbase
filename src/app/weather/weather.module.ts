import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SuiModule } from 'ng2-semantic-ui';

import { WeatherListComponent } from './weather-list/weather-list.component';
import { WeatherService } from './shared/services/weather.service';
import { WeatherRoutingModule } from './weather-routing.module';
import { WeatherDetailsComponent } from './weather-details/weather-details.component';

@NgModule({
  imports: [
    CommonModule,
    SuiModule,
    WeatherRoutingModule
  ],
  declarations: [
    WeatherListComponent,
    WeatherDetailsComponent
  ],
  providers: [
    WeatherService
  ]
})
export class WeatherModule { }
