import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../shared/services/weather.service';
import { RootForecast } from '../shared/model/root-forecast.model';
import { City } from '../shared/model/city.model';

@Component({
	selector: 'app-weather-details',
	templateUrl: './weather-details.component.html',
	styleUrls: ['./weather-details.component.css']
})
export class WeatherDetailsComponent implements OnInit {

	forecastDetail: RootForecast = <RootForecast>{};

	constructor(
		private router: Router,
		private route: ActivatedRoute,
		private weatherService: WeatherService
	) { 
		this.forecastDetail.city = <City>{};
	}

	ngOnInit() {
		this.route.queryParams.subscribe((queryParams: any) => {
			this.weatherService.onGetForecastNextHours( queryParams['city'], queryParams['country']).subscribe(
				(data: RootForecast) => {

					this.forecastDetail = data;

				}
			);

		});
	}


	onBack() {
        this.router.navigate(['/']);
    }

}
