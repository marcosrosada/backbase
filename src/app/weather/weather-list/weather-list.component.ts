import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { RootWeather } from '../shared/model/root-weather.model';
import { WeatherService } from '../shared/services/weather.service';

@Component({
	selector: 'app-weather-list',
	templateUrl: './weather-list.component.html',
	styleUrls: ['./weather-list.component.css']
})
export class WeatherListComponent implements OnInit {

	weatherList: RootWeather[] = [];

	constructor(
		private router: Router,
		private weatherService: WeatherService
	) { }


	ngOnInit() {
		this.onGetWeather('Amsterdam', 'nl');
		this.onGetWeather('Berlin', 'de');
		this.onGetWeather('Brussels', 'be');
		this.onGetWeather('London', 'uk');
		this.onGetWeather('Paris', 'fr');
	}


	onGetWeather(city: string, country: string) {
		this.weatherService.onGetWeatherList(city, country)
			.subscribe((data: RootWeather) => {
				this.weatherList.push(data);
			});
	}


	onDetails(item: RootWeather) {
		this.router.navigate([`weather-forecast-details`], {
            queryParams: { 'city': item.name, 'country': item.sys.country }
        });
	}

}
