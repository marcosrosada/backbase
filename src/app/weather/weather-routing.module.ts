import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WeatherListComponent } from './weather-list/weather-list.component';
import { WeatherDetailsComponent } from './weather-details/weather-details.component';

const weatherRouter: Routes = [
    { path: '', component: WeatherListComponent },
    { path: 'weather-forecast-details', component: WeatherDetailsComponent }
  ];

@NgModule({
  imports: [RouterModule.forChild(weatherRouter)],
  exports: [RouterModule]
})
export class WeatherRoutingModule { }