export const environment = {
  production: true,
  VERSION: require('../../package.json').version,
  API_URL: 'http://api.openweathermap.org/data/2.5/',
  APP_ID: '3d8b309701a13f65b660fa2c64cdc517'
};
