# BACKBASE 
Weather App

First of all, this project was developed with technologies that need to run on a server. If you have NodeJS, the easiest way would be to install the **[http-server](https://www.npmjs.com/package/http-server)** ( **npm install http-server -g** ) and open the terminal in the folder project and execute ...
> http-server

The server will be available on:
  > http://192.168.0.19:8080 or http://127.0.0.1:8080

Just open your browser and enjoy!

# About the Project
The home page has a list of 5 cities from europe with some information about the weather and when click in some item, you can see details of the selected city